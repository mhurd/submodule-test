SHELL := /bin/bash

-include ./mediawiki-docker-make/Makefile

.DEFAULT: install
.PHONY: install
install:
	@make preparesubmodule;
	@make freshinstall;

.PHONY: preparesubmodule
preparesubmodule:
	-@if [ ! -f "./mediawiki-docker-make/Makefile" ]; then \
		git submodule update --init --recursive; \
	fi
